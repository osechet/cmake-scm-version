
include(${CMAKE_CURRENT_LIST_DIR}/SCMTools.cmake)

#
# Calculate the version from SCM metadata. This function will detect your SCM and get the metadata
# accordingly. The variables given as parameters will be set with the calculated values.
#
function(scm_version VERSION MAJOR MINOR PATCH PRE_RELEASE BUILD)
    check_scm(SCM)

    if(SCM STREQUAL "git")
        include(${CMAKE_CURRENT_LIST_DIR}/GitGetVersion.cmake)
        git_scm_version(version major minor patch pre_release build)
    else()
        message(FATAL_ERROR "Unsupported SCM")
    endif()

    set(${VERSION} ${version} PARENT_SCOPE)
    set(${MAJOR} ${major} PARENT_SCOPE)
    set(${MINOR} ${minor} PARENT_SCOPE)
    set(${PATCH} ${patch} PARENT_SCOPE)
    set(${PRE_RELEASE} ${pre_release} PARENT_SCOPE)
    set(${BUILD} ${build} PARENT_SCOPE)
endfunction()
