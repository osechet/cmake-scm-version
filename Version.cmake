#
# Helper functions to manage versions.
#
# Do not include this file directly. It is used internally to handle versions.
#

#
# Guess the next version from the version's parts.
#
function(guess_next_version VERSION MAJOR MINOR PATCH PRE_RELEASE NEXT_VERSION)
    if(PRE_RELEASE)
        string(REGEX MATCHALL "[^.]+|[0-9]+$" pre_release_parts ${PRE_RELEASE})

        list(GET pre_release_parts 0 type)

        list(LENGTH pre_release_parts pre_release_parts_len)
        if (pre_release_parts_len GREATER 1)
            list(GET pre_release_parts 1 number)
        else()
            set(number -1)
        endif()

        string(REGEX MATCHALL ".*${MAJOR}\\.${MINOR}\\.${PATCH}" VERSION_ROOT ${VERSION})
        math(EXPR next "${number} + 1")
        set(${NEXT_VERSION} ${VERSION_ROOT}-${type}.${next} PARENT_SCOPE)
    else()
        string(REGEX MATCHALL ".*${MAJOR}\\.${MINOR}" VERSION_ROOT ${VERSION})
        math(EXPR next "${PATCH} + 1")
        set(${NEXT_VERSION} ${VERSION_ROOT}.${next} PARENT_SCOPE)
    endif()
endfunction()
