#
# Provides function to work with SCM.
#
# Do not include this file directly. It is used internally to manage SCM tools.
#

#
# Test if current directory is in a git repository.
#
function(is_git RESULT)
    find_package(Git QUIET)
    execute_process(
        COMMAND ${GIT_EXECUTABLE} rev-parse --is-inside-work-tree
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        RESULT_VARIABLE res
        OUTPUT_QUIET ERROR_QUIET
    )
    if(NOT res EQUAL 0)
        set(${RESULT} False PARENT_SCOPE)
    else()
        set(${RESULT} True PARENT_SCOPE)
    endif()
endfunction()

#
# Check which SCM is used.
#
function(check_scm RESULT)
    is_git(IS_GIT)
    if(IS_GIT)
        set(${RESULT} "git" PARENT_SCOPE)
    else()
        set(${RESULT} "unknown" PARENT_SCOPE)
    endif()
endfunction()
