cmake-scm-version
---

This project provides CMake scripts that calculates the version of a project from the SCM metadata. It is inspired by the Python [setuptools-scm](https://pypi.org/project/setuptools-scm/) package.

The scripts takes a look at three things:
* latest tag (with a version number)
* the distance to this tag (e.g. number of revisions since latest tag)
* workdir state (e.g. uncommitted changes since latest tag)

and uses roughly the following logic to render the version:

**no distance and clean**: ``{tag}``

**distance and clean**: ``{next_version}.dev{distance}+{revision hash}``

**no distance and not clean**: ``{tag}+dYYYMMMDD``

**distance and not clean**: ``{next_version}.dev{distance}+{revision hash}.dYYYMMMDD``

The next version is calculated by adding 1 to the last numeric component of the tag.

Note: Only git is currently supported.

## In a CMake project

To use the scripts in a CMake project, include the ``SCMGetVersion.cmake`` in your ``CMakeLists.txt`` and call the ``scm_version()`` function:

```
include(SCMGetVersion.cmake)

scm_version(VERSION MAJOR MINOR PATCH PRE_RELEASE BUILD)

message("Version: ${VERSION}")
```

The function will set the given variables with the calculated values.

## On the command line

To calculate the version directly from the command line, the project provides the ``GetVersion.cmake`` script. To use it, simply use the ``cmake -P`` syntax:

```
cmake -P GetVersion.cmake
```

The version will be printed in the standard output.
