#
# Git implementation for SCMGetVersion.
#
# Do not include this file directly, include SCMGetVersion.cmake instead.
#

include(${CMAKE_CURRENT_LIST_DIR}/Version.cmake)

#
# Get git metadata used to calculate the version.
#
function(_git_metadata VERSION DISTANCE GIT_SHA IS_DIRTY)
    find_package(Git REQUIRED QUIET)

    # Get working area state
    execute_process(COMMAND ${GIT_EXECUTABLE} status --porcelain --untracked-files=no
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        OUTPUT_VARIABLE status
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    if(status)
        # The output is not empty
        set(${IS_DIRTY} True PARENT_SCOPE)
    else()
        # The output is empty
        set(${IS_DIRTY} False PARENT_SCOPE)
    endif()

    # Get last tag
    execute_process(COMMAND ${GIT_EXECUTABLE} describe --abbrev=0 --tags
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        RESULT_VARIABLE res
        OUTPUT_VARIABLE version
        OUTPUT_STRIP_TRAILING_WHITESPACE
        ERROR_QUIET
    )
    if(NOT res EQUAL 0)
        # There is not tag
        set(${VERSION} "0.0.0" PARENT_SCOPE)

        # Count all nodes to get distance
        execute_process(COMMAND ${GIT_EXECUTABLE} rev-list HEAD --count
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            RESULT_VARIABLE res
            OUTPUT_VARIABLE distance
            OUTPUT_STRIP_TRAILING_WHITESPACE
            ERROR_QUIET
        )
        if(NOT res EQUAL 0)
            # There is no commit
            set(${DISTANCE} 0 PARENT_SCOPE)
            set(${GIT_SHA} "None" PARENT_SCOPE)
        else()
            set(${DISTANCE} ${distance} PARENT_SCOPE)

            # Get current commit SHA from git
            execute_process(COMMAND ${GIT_EXECUTABLE} rev-parse --short HEAD
                WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                OUTPUT_VARIABLE git_sha
                OUTPUT_STRIP_TRAILING_WHITESPACE
                ERROR_QUIET
            )
            set(${GIT_SHA} ${git_sha} PARENT_SCOPE)
        endif()
        return()
    endif()
    set(${VERSION} ${version} PARENT_SCOPE)

    # Get distance from last tag
    execute_process(COMMAND ${GIT_EXECUTABLE} rev-list master ${version}^..HEAD --count
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        OUTPUT_VARIABLE distance
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    # The command always include current commit in its count
    math(EXPR distance "${distance} - 1")
    set(${DISTANCE} ${distance} PARENT_SCOPE)

    # Get current commit SHA from git
    execute_process(COMMAND ${GIT_EXECUTABLE} rev-parse --short HEAD
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        OUTPUT_VARIABLE git_sha
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    set(${GIT_SHA} ${git_sha} PARENT_SCOPE)
endfunction()

#
# Calculate the version from git metadata.
#
function(git_scm_version VERSION MAJOR MINOR PATCH PRE_RELEASE BUILD)
    _git_metadata(version distance git_sha is_dirty)

    # Get partial versions into a list
    string(REGEX MATCHALL "-.*$|[0-9]+" partial_version_list ${version})

    # Set the version numbers
    list(GET partial_version_list 0 major)
    list(GET partial_version_list 1 minor)
    list(GET partial_version_list 2 patch)

    # The tweak part is optional, so check if the list contains it
    list(LENGTH partial_version_list partial_version_list_len)
    if (partial_version_list_len GREATER 3)
        list(GET partial_version_list 3 pre_release)
        string(SUBSTRING ${pre_release} 1 -1 pre_release)
    endif()

    string(TIMESTAMP date "%Y%m%d")

    if(distance EQUAL 0)
        if(NOT is_dirty)
            set(build "")
            set(full_version ${version})
        else()
            set(build "d${date}")
            set(full_version ${version}+${build})
        endif()
    else()
        guess_next_version(${version} ${major} ${minor} ${patch} "${pre_release}" next_version)
        if(pre_release)
            set(sep ".")
        else()
            set(sep "-")
        endif()
        if(NOT is_dirty)
            set(build "${git_sha}")
            set(full_version ${next_version}${sep}dev${distance}+${build})
        else()
            set(build "${git_sha}.d${date}")
            set(full_version ${next_version}${sep}dev${distance}+${build})
        endif()
    endif()

    set(${VERSION} ${full_version} PARENT_SCOPE)
    set(${MAJOR} ${major} PARENT_SCOPE)
    set(${MINOR} ${minor} PARENT_SCOPE)
    set(${PATCH} ${patch} PARENT_SCOPE)
    set(${PRE_RELEASE} ${pre_release} PARENT_SCOPE)
    set(${BUILD} ${build} PARENT_SCOPE)
endfunction()
