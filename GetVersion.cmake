#
# A command-line script that prints the calculated version in the standard output.
#
# Use it with the command `cmake -P GetVersion.cmake`.
#

include(${CMAKE_CURRENT_LIST_DIR}/SCMGetVersion.cmake)

scm_version(VERSION MAJOR MINOR PATCH PRE_RELEASE BUILD)

if(NOT VERSION)
    message("No version found")
else()
    message(${VERSION})
endif()
