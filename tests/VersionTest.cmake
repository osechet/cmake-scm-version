include(${CMAKE_CURRENT_LIST_DIR}/Tests.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/../Version.cmake)

guess_next_version("1.2.3" 1 2 3 "" next)
assert_eq("1.2.4" ${next})

guess_next_version("1.2.3-rc" 1 2 3 rc next)
assert_eq("1.2.3-rc.0" ${next})

guess_next_version("1.2.3-rc.1" 1 2 3 rc.1 next)
assert_eq("1.2.3-rc.2" ${next})

guess_next_version("v1.2.3" 1 2 3 "" next)
assert_eq("v1.2.4" ${next})

guess_next_version("v1.2.3-rc" 1 2 3 rc next)
assert_eq("v1.2.3-rc.0" ${next})

guess_next_version("v1.2.3-rc.1" 1 2 3 rc.1 next)
assert_eq("v1.2.3-rc.2" ${next})

message("Tests succeeded")
