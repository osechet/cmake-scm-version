
function(assert_eq expected result)
    if(NOT ${result} STREQUAL ${expected})
        message(FATAL_ERROR "Expected ${expected}, got ${result}")
    endif()
endfunction()
